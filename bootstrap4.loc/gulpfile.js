var gulp = require('gulp'),sass = require('gulp-sass'),  watch = require('gulp-watch');

gulp.task('default', function() {
    // place code for your default task here
});

gulp.task('hello', function() {
    console.log('Hello Zell');
});

gulp.task('sass', function(){
    gulp.src('app/sass/main.sass')
        .pipe(sass()) // Using gulp-sass
        .pipe(gulp.dest('app/css'))
});

gulp.task('stream', function () {
    // Endless stream mode
    return watch('app/sass/**/*.sass', { ignoreInitial: false })
        .pipe(gulp.dest('build'));
});

gulp.task('callback', function () {
    // Callback mode, useful if any plugin in the pipeline depends on the `end`/`flush` event
    return watch('css/**/*.css', function () {
        gulp.src('css/**/*.css')
            .pipe(gulp.dest('build'));
    });
});