<?php
return array (
  'mailer' => 
  array (
    'recipients.form' => 
    array (
      0 => 'contacts',
    ),
  ),
  'contacts' => 
  array (
    'delete' => 
    array (
      0 => 'contacts',
      1 => 'shop',
      2 => 'team',
    ),
    'contacts_collection' => 
    array (
      0 => 'contacts',
      1 => 'shop',
      2 => 'team',
    ),
    'links' => 
    array (
      0 => 'shop',
    ),
    'profile.tab' => 
    array (
      0 => 'shop',
      1 => 'team',
    ),
    'explore' => 
    array (
      0 => 'shop',
    ),
    'merge' => 
    array (
      0 => 'shop',
    ),
  ),
  'shop' => 
  array (
    'backend_customers_list' => 
    array (
      0 => 'contacts',
    ),
  ),
  'site' => 
  array (
    'update.route' => 
    array (
      0 => 'shop',
    ),
  ),
  'webasyst' => 
  array (
    'backend_dispatch_miss' => 
    array (
      0 => 'team',
    ),
    'backend_personal_profile' => 
    array (
      0 => 'team',
    ),
  ),
);
