<?php
return array (
  'id' => 
  array (
    'type' => 'int',
    'params' => '11',
    'unsigned' => 1,
    'null' => 0,
    'autoincrement' => 1,
  ),
  'name' => 
  array (
    'type' => 'varchar',
    'params' => '255',
  ),
  'create_datetime' => 
  array (
    'type' => 'datetime',
    'null' => 0,
  ),
);
