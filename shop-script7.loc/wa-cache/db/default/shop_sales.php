<?php
return array (
  'hash' => 
  array (
    'type' => 'varchar',
    'params' => '32',
    'null' => 0,
  ),
  'date' => 
  array (
    'type' => 'date',
    'null' => 0,
  ),
  'name' => 
  array (
    'type' => 'varchar',
    'params' => '255',
    'null' => 0,
    'default' => '',
  ),
  'order_count' => 
  array (
    'type' => 'int',
    'params' => '11',
    'null' => 0,
    'default' => '0',
  ),
  'sales' => 
  array (
    'type' => 'float',
    'null' => 0,
    'default' => '0',
  ),
  'shipping' => 
  array (
    'type' => 'float',
    'null' => 0,
    'default' => '0',
  ),
  'tax' => 
  array (
    'type' => 'float',
    'null' => 0,
    'default' => '0',
  ),
  'purchase' => 
  array (
    'type' => 'float',
    'null' => 0,
    'default' => '0',
  ),
  'cost' => 
  array (
    'type' => 'float',
    'null' => 0,
    'default' => '0',
  ),
  'new_customer_count' => 
  array (
    'type' => 'int',
    'params' => '11',
    'null' => 0,
    'default' => '0',
  ),
);
