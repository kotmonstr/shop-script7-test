<?php
class testBackendActions extends waViewActions
{
    public function defaultAction()
    {
        if (waRequest::method() == 'get') {
            $limit = waRequest::get('limit') ? waRequest::get('limit') : 10;
        }

        $model = new productModel();
        $records = $model->order('id ASC')->limit($limit)->fetchAll();
        $all = count($model->order('id ASC')->fetchAll());

        $this->view->assign(['records'=> $records,'limit'=> $limit,'all'=> $all ]);
    }
}