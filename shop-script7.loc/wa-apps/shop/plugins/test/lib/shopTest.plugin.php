<?php


class shopTestPlugin extends shopPlugin
{

    public function backendMenu()
    {
        return array(
            'core_li' => '<li class="no-tab"><a href="/webasyst/test/">'._wp('Тестовое задание').'</a></li>',
        );

    }

}